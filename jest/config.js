const ignorePaths = ['<rootDir>/.yarn-cache', '<rootDir>/node_modules/'];

module.exports = {
  collectCoverageFrom: ['{app,core}/**/*.{ts,tsx,js}'],
  coveragePathIgnorePatterns: [...ignorePaths, '<rootDir>/.*.d.ts'],
  coverageDirectory: '<rootDir>/.coverage',
  coverageReporters: ['lcov', 'text-summary'],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.test.json',
    },
    __DEV__: true,
  },
  testURL: 'http://localhost/',
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  moduleNameMapper: {
    '\\.(woff2|ttf|eot|svg|png|jpe?g|gif)$': '<rootDir>/jest/mocks/resourceMock.js',
    '\\.css$': 'identity-obj-proxy',
    '^assets.*$': '<rootDir>/jest/mocks/resourceMock.js',
    '^core/(.*)$': '<rootDir>/core/$1',
  },
  modulePaths: ['<rootDir>/app'],
  resetModules: true,
  rootDir: '../',
  setupFiles: ['<rootDir>/jest/environment.js'],
  testRegex: '/(app|core|scripts|webpack)/.*(.+\\.)?spec\\.tsx?$',
  testPathIgnorePatterns: ignorePaths,
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  transformIgnorePatterns: [],
  verbose: true,
};
