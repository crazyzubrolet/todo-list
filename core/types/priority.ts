export enum Priority {
  NONE = 0,
  LOW,
  MEDIUM,
  HIGH,
}
