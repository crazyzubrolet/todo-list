export enum Status {
  INITIAL = 'empty',
  LOADING = 'loading',
  UPDATING = 'updating',
  LOADED = 'loaded',
  ERROR = 'error',
}
