import React from 'react';
import cn from 'classnames';

import { noop } from 'core/utils/noop';

import styles from './styles.css';

interface IDefaultProps {
  type: string;
  value: string;
  placeholder: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
}

interface IProps
  extends IDefaultProps,
    Omit<
      React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
      'type' | 'value' | 'placeholder' | 'onChange'
    > {
  name?: string;
  className?: string;
}

const Input = ({ className, ...restProps }: IProps) => (
  <input {...restProps} className={cn(styles.input, className)} data-testid="input" />
);

const defaultProps: IDefaultProps = {
  type: 'text',
  value: '',
  placeholder: 'Insert your text here',
  onChange: noop,
};

Input.defaultProps = defaultProps;

export default Input;
