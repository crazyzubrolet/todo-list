import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import 'jest-dom/extend-expect';

import Input from './Input';

describe('Input', () => {
  afterEach(cleanup);

  it('Renders Input correctly', () => {
    const actual = render(
      <Input name="input" value="Random text string" placeholder="temp text" className="test" />
    );

    expect(actual.asFragment()).toMatchSnapshot();
  });

  it('Calls onChange handler', done => {
    const newValue = 'test';

    const handler: React.ChangeEventHandler<HTMLInputElement> = jest.fn(event => {
      expect(event.target.value).toBe(newValue);
      done();
    });

    const wrapper = render(<Input onChange={handler} />);
    const inputElement = wrapper.getByTestId('input') as HTMLInputElement;

    expect(handler).not.toHaveBeenCalled();

    fireEvent.change(inputElement, { target: { value: newValue } });
    expect(handler).toHaveBeenCalledTimes(1);
  });
});
