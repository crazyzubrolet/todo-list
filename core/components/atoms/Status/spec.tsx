import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import 'jest-dom/extend-expect';

import { Priority } from 'core/types/priority';

import Status from './Status';

describe('Status', () => {
  afterEach(cleanup);

  it('Renders Status correctly', () => {
    const actual = render(<Status id="test" done priority={Priority.HIGH} className="test" />);

    expect(actual.asFragment()).toMatchSnapshot();
  });

  it('Calls onChange handler', done => {
    const checked = false;

    const handler: React.ChangeEventHandler<HTMLInputElement> = jest.fn(event => {
      expect(event.target.checked).toBe(!checked);
      done();
    });

    const wrapper = render(<Status id="test" done={checked} onChange={handler} />);
    const checkbox = wrapper.getByTestId('status');

    expect(handler).not.toHaveBeenCalled();

    fireEvent.click(checkbox);
    expect(handler).toHaveBeenCalledTimes(1);
  });
});
