import React from 'react';
import cn from 'classnames';

import { Priority } from 'core/types/priority';
import { noop } from 'core/utils/noop';

import Icon from './icon';
import styles from './styles.css';

interface IDefaultProps {
  done: boolean;
  priority: Priority;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
}

interface IProps extends IDefaultProps {
  id: string;
  className?: string;
}

const getPriorityClassName = (props: IProps) => {
  if (props.done) {
    return styles.completed;
  }

  switch (props.priority) {
    case Priority.HIGH:
      return styles.priority_0;
    case Priority.MEDIUM:
      return styles.priority_1;
    case Priority.LOW:
      return styles.priority_2;
    default:
      return '';
  }
};

const Checkbox = (props: IProps) => {
  const { id, done, onChange, className } = props;

  return (
    <label
      htmlFor={id}
      className={cn(styles.label, className, getPriorityClassName(props))}
      data-testid="status"
    >
      <input
        id={id}
        checked={done}
        type="checkbox"
        className={styles.checkbox}
        onChange={onChange}
      />
      <Icon className={styles.icon} />
    </label>
  );
};

const defaultProps: IDefaultProps = {
  done: false,
  priority: Priority.NONE,
  onChange: noop,
};

Checkbox.defaultProps = defaultProps;

export default Checkbox;
