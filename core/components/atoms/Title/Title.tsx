import React from 'react';
import cn from 'classnames';

interface IDefaultProps {
  size: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
}

interface IProps extends IDefaultProps {
  children: string;
  className?: string;
}

const Title = ({ children, size, className }: IProps) =>
  React.createElement(
    size,
    {
      className: cn(className),
      'data-testid': 'title',
    },
    children
  );

const defaultProps: IDefaultProps = {
  size: 'h1',
};

Title.defaultProps = defaultProps;

export default Title;
