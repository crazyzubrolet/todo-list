import React from 'react';
import { render, cleanup } from 'react-testing-library';
import 'jest-dom/extend-expect';

import Title from './Title';

describe('Title', () => {
  afterEach(cleanup);

  it('Should render h1 by default with desired text', () => {
    const title = 'Sample text';
    const actual = render(<Title>{title}</Title>);

    expect(actual.asFragment()).toMatchSnapshot();
    expect(actual.getByTestId('title')).toHaveTextContent(title);
  });

  it('Should render correct with props', () => {
    const className = 'test-classname';
    const titleElement = 'h3';

    const actual = render(
      <Title size={titleElement} className={className}>
        Plain text
      </Title>
    ).getByTestId('title');

    expect(actual).toHaveClass(className);
    expect(actual.tagName.toLowerCase()).toBe(titleElement);
  });
});
