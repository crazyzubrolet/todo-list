import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';

import Select from './Select';

describe('Select', () => {
  afterEach(cleanup);

  it('Renders Select correctly', () => {
    const actual = render(<Select values={['Test', 'test2', 'Test some text']} />);

    expect(actual.asFragment()).toMatchSnapshot();
  });

  it('Renders correctly with props', () => {
    const actual = render(
      <Select
        placeholder="Select value"
        values={['Value 1', 'Value 2']}
        className="test"
        onSelect={jest.fn}
      />
    );

    expect(actual.asFragment()).toMatchSnapshot();
  });

  it('Calls onChange handler', () => {
    const handler: React.ChangeEventHandler<HTMLSelectElement> = jest.fn(event => {
      expect(event.target.value).toBe('test2');
    });

    const wrapper = render(<Select values={['test', 'test2']} onSelect={handler} />);
    const select = wrapper.getByTestId('select');

    expect(handler).not.toHaveBeenCalled();

    fireEvent.change(select, { target: { value: 'test2' } });
    expect(handler).toHaveBeenCalledTimes(1);
  });
});
