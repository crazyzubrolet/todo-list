import React from 'react';
import cn from 'classnames';

import { noop } from 'core/utils/noop';

import styles from './styles.css';

interface IDefaultProps {
  onSelect: React.ChangeEventHandler<HTMLSelectElement>;
}

interface IProps extends IDefaultProps {
  values: Array<string | number>;
  placeholder?: string;
  className?: string;
}

const Select = ({ values, placeholder, onSelect, className }: IProps) => (
  <select
    className={cn(styles.select, className)}
    onChange={onSelect}
    data-testid="select"
    defaultValue={placeholder}
  >
    {placeholder && (
      <option value="" disabled>
        {placeholder}
      </option>
    )}
    {values.map(value => (
      <option key={value} value={value}>
        {value}
      </option>
    ))}
  </select>
);

const defaultProps: IDefaultProps = {
  onSelect: noop,
};

Select.defaultProps = defaultProps;

export default Select;
