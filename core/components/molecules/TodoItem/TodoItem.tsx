import React from 'react';
import cn from 'classnames';

import { Status } from 'core/components/atoms/Status';
import { Title } from 'core/components/atoms/Title';
import { noop } from 'core/utils/noop';
import { Priority } from 'core/types/priority';

import styles from './styles.css';

interface IDefaultProps {
  done: boolean;
  onResolve(newStatus: boolean): void;
}

interface IProps extends IDefaultProps {
  id: string;
  title: string;
  dueDate: string;
  priority?: Priority;
  className?: string;
}

interface IState {
  resolved: boolean;
}

class TodoItem extends React.PureComponent<IProps, IState> {
  public static defaultProps: IDefaultProps = {
    done: false,
    onResolve: noop,
  };

  public readonly state: IState = {
    resolved: this.props.done,
  };

  private handleStatusChange: ComponentProps<typeof Status>['onChange'] = event => {
    this.setState(
      {
        resolved: event.target.checked,
      },
      () => this.props.onResolve(this.state.resolved)
    );
  };

  public render() {
    const { id, title, dueDate: tillDate, priority, className } = this.props;
    const { resolved } = this.state;

    return (
      <div
        className={cn(styles.todoItem, className, {
          [styles.completed]: resolved,
        })}
      >
        <Status id={id} done={resolved} priority={priority} onChange={this.handleStatusChange} />
        <Title size="h3" className={styles.title}>
          {title}
        </Title>
        <div className={styles.dueDate}>{tillDate}</div>
      </div>
    );
  }
}

export default TodoItem;
