import React from 'react';
import { render, cleanup } from 'react-testing-library';

import { Priority } from 'core/types/priority';

import TodoItem from './TodoItem';

describe('TodoItem', () => {
  afterEach(cleanup);

  it('Renders default correctly', () => {
    const actual = render(<TodoItem id="test" title="Test" dueDate={new Date(0).toDateString()} />);

    expect(actual.asFragment()).toMatchSnapshot();
  });

  it('Renders with props correctly', () => {
    const actual = render(
      <TodoItem
        done
        id="test"
        priority={Priority.HIGH}
        title="New title"
        dueDate={new Date(0).toDateString()}
        className="test"
      />
    );

    expect(actual.asFragment()).toMatchSnapshot();
  });
});
