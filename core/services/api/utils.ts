import * as R from 'ramda';

import { omitEmptyKeys } from 'core/utils/object';

import { HttpStatusCode, ApiResponseType } from './types';

export function getResponseStatusType(status: HttpStatusCode): ApiResponseType {
  switch (true) {
    case status >= 200 && status < 400:
      return ApiResponseType.SUCCESS;
    case status >= 400 && status < 500:
      return ApiResponseType.CLIENT_ERROR;
    case status >= 500 && status < 600:
      return ApiResponseType.SERVER_ERROR;
    default:
      return ApiResponseType.UNKNOWN;
  }
}

const getStringFromObject = R.compose(
  R.join('&'),
  R.map(R.join('=')),
  Object.entries,
  R.map(prop => (Array.isArray(prop) ? R.join(',', prop) : prop))
);

export const getQueryString = (path: string, params?: object) =>
  `${path}?${getStringFromObject(omitEmptyKeys(params))}`.replace(/\?$/, '');
