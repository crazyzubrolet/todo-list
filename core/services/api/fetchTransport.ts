import {
  ITransport,
  IRequestOptions,
  ITransportSuccess,
  HttpStatusCode,
  ITransportError,
  ApiResponseType,
  ApiErrorType,
} from './types';
import { getResponseStatusType } from './utils';

interface IOptions {
  basePath: string;
}

const defaultHeaders = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

export default class FetchTransport implements ITransport {
  private basePath: string;

  public static create(options: IOptions): FetchTransport {
    return new FetchTransport(options);
  }

  constructor(options: IOptions) {
    this.basePath = options.basePath;
  }

  public sendRequest(options: IRequestOptions) {
    const request = this.createRequest(options);

    return fetch(request).then(this.processResponse, error => {
      const errorResponse: ITransportError = {
        type: ApiResponseType.UNKNOWN,
        description: 'Fetch error occured',
        error,
      };

      return Promise.reject(errorResponse);
    });
  }

  private createRequest = (options: IRequestOptions): Request => {
    const { path, method, headers, body } = options;

    return new Request(`${this.basePath}${path}`, {
      method,
      headers: new Headers({
        ...defaultHeaders,
        ...headers,
      }),
      body: JSON.stringify(body),
    });
  };

  private createSuccessResponse = (body: any): ITransportSuccess => ({
    type: ApiResponseType.SUCCESS,
    data: body,
  });

  private createErrorResponse = (status: HttpStatusCode, body: any): ITransportError => ({
    type: getResponseStatusType(status) as ApiErrorType,
    response: {
      status,
      body,
    },
    description: body && body.description,
  });

  private processResponse = (response: Response): Promise<ITransportSuccess | ITransportError> =>
    response.json().then<any>(body => {
      if (getResponseStatusType(response.status) === ApiResponseType.SUCCESS) {
        return this.createSuccessResponse(body);
      }

      return Promise.reject(this.createErrorResponse(response.status, body));
    });
}
