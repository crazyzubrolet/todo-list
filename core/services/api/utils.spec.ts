import { HttpStatusCode, ApiResponseType } from './types';
import { getResponseStatusType, getQueryString } from './utils';

describe('Utils', () => {
  describe('getResponseStatusType', () => {
    it('Should return correct type by code', () => {
      expect(getResponseStatusType(HttpStatusCode.OK)).toBe(ApiResponseType.SUCCESS);

      expect(getResponseStatusType(HttpStatusCode.BAD_REQUEST)).toBe(ApiResponseType.CLIENT_ERROR);

      expect(getResponseStatusType(500)).toBe(ApiResponseType.SERVER_ERROR);

      expect(getResponseStatusType(1337)).toBe(ApiResponseType.UNKNOWN);
    });
  });

  describe('getQueryString', () => {
    it('Should create query string from object', () => {
      const path = 'http://api.path.com';
      const queryParams = {
        some: 'value',
        with: 0,
        props: [],
        array: [1, 2, 3],
      };

      const expected = `${path}?some=value&with=0&array=1,2,3`;
      const actual = getQueryString(path, queryParams);

      expect(actual).toEqual(expected);
    });

    it('Should handle empty query params', () => {
      const path = 'http://api.path.com';

      const expected = path;
      const actual = getQueryString(path);

      expect(actual).toEqual(expected);
    });
  });
});
