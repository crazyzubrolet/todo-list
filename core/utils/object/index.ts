import * as R from 'ramda';

export const omitEmptyKeys = R.pickBy(R.complement(R.anyPass([Number.isNaN, R.isNil, R.isEmpty])));

export const areEqual = R.anyPass([R.identical, R.equals]);

export function createComparator<T = any>(filter?: Array<Extract<keyof T, string>>) {
  return (a: T, b: T): boolean =>
    filter ? areEqual(R.pick(filter, a), R.pick(filter, b)) : areEqual(a, b);
}

export function groupById<T extends any, ID extends keyof T, SK extends string>(
  list: T[],
  options: {
    id?: ID;
    subKey?: SK;
  } = {}
) {
  const { id = 'id', subKey } = options;

  const reducer = R.ifElse(
    (key?: string) => !!key,
    (key: SK) => (acc: Record<string, T>, value: T) => ({ ...acc, [String(key)]: value }),
    () => (_acc: Record<string, T>, next: T) => next
  )(subKey);

  return R.reduceBy(reducer, {}, (obj: T) => obj[id])(list) as Record<
    T[ID],
    // Maybe { [key in SK]?: T } is redundant -> { [key in SK]?: T }
    SK extends string ? { [key in SK]?: T } : T
  >;
}
