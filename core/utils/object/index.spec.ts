import { omitEmptyKeys, areEqual, createComparator, groupById } from '.';

describe('object utils', () => {
  const obj = {
    zero: 0,
    nonZero: 10,
    bool1: true,
    bool2: false,
    array: [1, 2, 3],
    empty1: '',
    empty2: [],
    empty3: undefined,
    empty4: null,
    empty5: NaN,
    string: 'text',
  };

  const objWithoutEmptyKeys = {
    zero: 0,
    nonZero: 10,
    array: [1, 2, 3],
    bool1: true,
    bool2: false,
    string: 'text',
  };

  describe('omitEmptyKeys', () => {
    it('`omitEmptyKeys` should remove empty fields from an object', () => {
      expect(omitEmptyKeys(obj)).toEqual(objWithoutEmptyKeys);
    });
  });

  describe('areEqual', () => {
    it('should return true if objects are shallow equal', () => {
      const array1 = [1, 2, 3];
      const array2 = array1;

      const obj1 = {
        temp: 'value',
      };
      const obj2 = obj1;

      expect(areEqual(array1, array2)).toBe(true);
      expect(areEqual(obj1, obj2)).toBe(true);
    });

    it('should return true if keys of objects are equal', () => {
      const array1 = [1, 2, 3];
      const array2 = [1, 2, 3];

      const obj1 = {
        temp: 'value',
      };
      const obj2 = {
        temp: 'value',
      };

      expect(areEqual(array1, array2)).toBe(true);
      expect(areEqual(obj1, obj2)).toBe(true);
    });

    it('should return false if keys of objects are different', () => {
      const array1 = [1, 2, 3];
      const array2 = [1, 2, 4];

      const obj1 = {
        has: 'value1',
      };
      const obj2 = {
        has: 'value2',
      };

      expect(areEqual(array1, array2)).toBe(false);
      expect(areEqual(obj1, obj2)).toBe(false);
    });
  });

  describe('createComparator', () => {
    it('If there is no filter `createComparator` should compare objects entirely', () => {
      const obj1 = {
        key1: 'test',
        key2: 42,
        key3: [],
      };
      const obj2 = {
        key1: 'test',
        key2: 42,
        key3: [],
      };

      const sameTypeComparator = createComparator<typeof obj1>();

      expect(sameTypeComparator(obj1, obj2)).toBe(true);

      const differentTypeComparator = createComparator();

      expect(differentTypeComparator(obj, objWithoutEmptyKeys)).toBe(false);
    });

    it('If there is a filter `createComparator` should compare objects by picked keys', () => {
      const obj1 = {
        key1: 'test',
        key2: 13,
        key3: [],
      };
      const obj2 = {
        key1: 'test',
        key2: 37,
        key3: [],
      };

      const sameTypeComparatorTruthy = createComparator<typeof obj1>(['key1', 'key3']);

      expect(sameTypeComparatorTruthy(obj1, obj2)).toBe(true);

      const differentTypeComparator = createComparator<typeof objWithoutEmptyKeys>([
        'array',
        'bool1',
        'bool2',
        'nonZero',
        'string',
        'zero',
      ]);

      expect(differentTypeComparator(obj, objWithoutEmptyKeys)).toBe(true);

      const sameTypeComparatorFalsy = createComparator<typeof obj1>(['key1', 'key2']);

      expect(sameTypeComparatorFalsy(obj1, obj2)).toBe(false);
    });
  });

  describe('groupById', () => {
    const arrayOfObj = [
      { id: 13, text: 'test1' },
      { id: 37, text: 'test2' },
      { id: 42, text: 'test3' },
    ];

    it('by default should return object where keys are object `id` and values are the entire objects', () => {
      const expected = {
        [arrayOfObj[0].id]: arrayOfObj[0],
        [arrayOfObj[1].id]: arrayOfObj[1],
        [arrayOfObj[2].id]: arrayOfObj[2],
      };

      expect(groupById(arrayOfObj)).toEqual(expected);
    });

    it('If subkey is passed then should place all data by subkey in the key', () => {
      const key = 'data';
      const expected = {
        [arrayOfObj[0].id]: {
          [key]: arrayOfObj[0],
        },
        [arrayOfObj[1].id]: {
          [key]: arrayOfObj[1],
        },
        [arrayOfObj[2].id]: {
          [key]: arrayOfObj[2],
        },
      };

      expect(
        groupById(arrayOfObj, {
          subKey: key,
        })
      ).toEqual(expected);
    });

    it('Should be able to group objects by custom field', () => {
      const expected = {
        [arrayOfObj[0].text]: arrayOfObj[0],
        [arrayOfObj[1].text]: arrayOfObj[1],
        [arrayOfObj[2].text]: arrayOfObj[2],
      };

      expect(
        groupById(arrayOfObj, {
          id: 'text',
        })
      ).toEqual(expected);
    });
  });
});
