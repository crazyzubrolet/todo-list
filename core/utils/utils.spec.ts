import { noop, nullNoop } from './noop';
import { uniqueId } from './uniqueId';
import { isLeapYear } from './time';

describe('Core utils', () => {
  describe('noop utils', () => {
    it('`noop` should return nothing', () => {
      expect(noop()).toBeUndefined();
    });

    it('`nullNoop should return null', () => {
      expect(nullNoop()).toBeNull();
    });
  });

  describe('uniqueId', () => {
    it('should return new unique string every call', () => {
      expect(uniqueId()).not.toEqual(uniqueId());
    });

    it('should return prefixed id if argument was passed', () => {
      const prefix = 'prefix_';

      expect(uniqueId(prefix).includes(prefix)).toBe(true);
    });
  });

  describe('time', () => {
    it.each<[number, boolean]>([
      [2000, true],
      [2018, false],
      [2020, true],
      [2021, false],
      [2100, false],
      [2200, false],
      [2400, true],
    ])('isLeapYear should detect "%s" year is lear or not', (input, expected) => {
      expect(isLeapYear(input)).toBe(expected);
    });
  });
});
