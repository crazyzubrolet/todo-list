export const idCounter: Record<string, number> = {};

export const uniqueId = (prefix: string = 'unique_') => {
  if (!idCounter[prefix]) {
    idCounter[prefix] = 0;
  }

  idCounter[prefix] += 1;

  return `${prefix}${idCounter[prefix]}`;
};
