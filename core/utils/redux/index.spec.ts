import { handleAsyncAction, IAsyncHandlers } from '.';

describe('Async action handler', () => {
  it('should handle async action request', () => {
    const state = { async: 'request' };
    const handlers = { request: (s: any) => s };
    const requestSpy = jest.spyOn(handlers, 'request');
    const action = {
      type: 'async/request',
      meta: { isPending: true },
      payload: {},
    };
    const actual = handleAsyncAction(handlers)(state, action);

    expect(requestSpy).toHaveBeenCalledTimes(1);
    expect(requestSpy).toHaveBeenCalledWith(state, action);
    expect(actual).toEqual({ async: 'request' });
  });

  it('should handle async action reject', () => {
    const error = { error: 'failure' };
    const handlers: IAsyncHandlers<any, any> = { failure: (_, { payload }) => payload };
    const failureSpy = jest.spyOn(handlers, 'failure');
    const action = {
      type: 'async/failure',
      error: true,
      payload: error,
    };
    const actual = handleAsyncAction(handlers)({}, action);

    expect(failureSpy).toHaveBeenCalledTimes(1);
    expect(failureSpy).toHaveBeenCalledWith({}, action);
    expect(actual.error).toBe('failure');
  });

  it('should handle async action resolve', () => {
    const resolve = { resolve: 'success' };
    const handlers: IAsyncHandlers<any, any> = { success: (_, { payload }) => payload };
    const successSpy = jest.spyOn(handlers, 'success');
    const action = {
      type: 'async/success',
      payload: resolve,
    };
    const actual = handleAsyncAction(handlers)({}, action);

    expect(successSpy).toHaveBeenCalledTimes(1);
    expect(successSpy).toHaveBeenCalledWith({}, action);
    expect(actual.resolve).toBe('success');
  });

  it('should handle async action handlers not being specified', () => {
    const state = { state: 'dummy' };
    const actual = handleAsyncAction({})(state, {
      type: 'async/success',
    } as any);

    expect(actual).toEqual({ state: 'dummy' });
  });
});
