import { ActionMeta, Action, handleActions as handleActionsReducer } from 'redux-actions';
import { ActionCreatorsMapObject, Dispatch, bindActionCreators } from 'redux';
import { ThunkAction } from 'redux-thunk';

export type ActionPayload<A> = A extends ActionMeta<infer P1, any>
  ? (P1 extends Promise<any> ? P1 : A)
  : A extends Action<infer P2>
  ? (P2 extends Promise<any> ? P2 : A)
  : A;

/**
 * Retrieve payload type from `ThunkAction|ActionMeta|Action` if it's a Promise
 */
type ActionReturnType<A> = A extends ThunkAction<infer T, any, any, any>
  ? ActionPayload<T>
  : ActionPayload<A>;

/**
 * Retrive arguments types of a function
 */
type ArgumentTypes<T> = T extends (...args: infer U) => any ? U : never;

/**
 * Create a mapped type where keys are action names and values are actions
 * with replaced return types (async actions return promises retrieved from
 * their respective payloads)
 */
export type ActionsWithPromises<T> = {
  [K in keyof T]: T[K] extends (...args: any[]) => any
    ? (...a: ArgumentTypes<T[K]>) => ActionReturnType<ReturnType<T[K]>>
    : never
};

/**
 * Standard `bindActionCreators` typings with replaced return type
 */
export type bindActionsToPayloadReturns = <A, M extends ActionCreatorsMapObject<A>>(
  actionCreators: M,
  dispatch: Dispatch
) => ActionsWithPromises<M>;

/**
 * Use type casting to change return type of `bindActionCreators`
 */
export const bindActions = bindActionCreators as bindActionsToPayloadReturns;

export type IReducer<S, A> = (state: S, action: A) => S;

export interface IAsyncHandlers<S, A extends IAction> {
  request?: IReducer<S, A & { meta: { isPending: true } }>;
  success?: IReducer<S, A>;
  failure?: IReducer<S, A & { payload?: any; error: true }>;
}

export interface IAction {
  type: string;
  payload?: any;
}

const getActionHandler = <S, A>(handler?: IReducer<S, A>) => (state: S) => (action: A): S =>
  handler ? handler(state, action) : state;

const isErrorAction = <A extends IAction & { error?: any }>(
  action: A
): action is A & { error: true } => action.error;

const isRequestAction = <A extends IAction & { meta?: { isPending?: any } }>(
  action: A
): action is A & { meta: { isPending: true } } => action.meta && action.meta.isPending;

/**
 * Async action handler return essentially a reducer by action type
 */
export const handleAsyncAction = <S, A extends IAction = IAction>(
  handlers: IAsyncHandlers<S, A>
): IReducer<S, A> => (state: S, action: A): S => {
  if (isErrorAction(action)) {
    return getActionHandler(handlers.failure)(state)(action);
  }

  if (isRequestAction(action)) {
    return getActionHandler(handlers.request)(state)(action);
  }

  return getActionHandler(handlers.success)(state)(action);
};

/**
 * ReducerMap has keys that correspond to action types and reducers with typed
 * arguments as values (extracted payload and meta from actions union)
 */
type IReducerMap<S, A extends IAction> = { [T in A['type']]: IReducer<S, Extract<A, { type: T }>> };

type IHandleActions = <S, A extends IAction>(
  reducerMap: IReducerMap<S, A>,
  initialState: S
) => IReducer<S, A>;

/**
 * Cast a new type for `handleActions`
 */
export const handleActions = handleActionsReducer as IHandleActions;

type OmitPayload<A extends IAction> = Omit<A, 'payload'>;

export type ActionPromise<A extends IAction> = OmitPayload<A> & { payload: Promise<A['payload']> };
