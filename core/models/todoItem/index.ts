import { Priority } from 'core/types/priority';

export interface ITodoItem {
  id: number;
  title: string;
  dueDate: number; // timestamp in ms
  priority: Priority;
  completed: boolean;
  description?: string;
}
