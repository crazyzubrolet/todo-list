import { ITodoItem } from 'core/models/todoItem';
import { Priority } from 'core/types/priority';

export interface IExternalTodoItem {
  id: number;
  title: string;
  due_date: number; // timestamp in sec
  priority: 'high' | 'medium' | 'low' | 'none';
  completed: boolean;
  description?: string;
}

const mapExternalPriority = (priority: IExternalTodoItem['priority']): Priority => {
  switch (priority) {
    case 'high':
      return Priority.HIGH;
    case 'medium':
      return Priority.MEDIUM;
    case 'low':
      return Priority.LOW;
    default:
      return Priority.NONE;
  }
};

export const mapPriorityToExternal = (priority: Priority): IExternalTodoItem['priority'] => {
  switch (priority) {
    case Priority.HIGH:
      return 'high';
    case Priority.MEDIUM:
      return 'medium';
    case Priority.LOW:
      return 'low';
    default:
      return 'none';
  }
};

export const mapExternalTodoItem = (data: IExternalTodoItem): ITodoItem => ({
  id: data.id,
  title: data.title,
  dueDate: data.due_date * 1000,
  priority: mapExternalPriority(data.priority),
  completed: data.completed,
  description: data.description,
});
