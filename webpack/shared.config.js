const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

const { ROOT_PATH, CLIENT_APP_PATH: APP_PATH, OUTPUT_PATH } = require('./environment');

module.exports = {
  context: ROOT_PATH,
  entry: {
    app: [path.resolve(APP_PATH, 'index.tsx')],
  },

  stats: {
    colors: true,
    warningsFilter: /export .* was not found in/,
  },

  output: {
    path: OUTPUT_PATH,
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[hash].js',
    sourceMapFilename: '[filebase].map',
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'cache-loader',
            options: {
              cacheDirectory: path.join(ROOT_PATH, '.ts-cache'),
            },
          },
          {
            loader: 'thread-loader',
            options: {
              workers: process.env.WEBPACK_WORKERS || 1,
            },
          },
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              happyPackMode: true,
            },
          },
        ],
      },
      {
        test: /\.(eot|ttf|woff2?)(\?v=\d+\.\d+\.\d+)?$/,
        use: 'file-loader',
      },
    ],
  },

  optimization: {
    splitChunks: {
      maxInitialRequests: 5,
      automaticNameDelimiter: '-',
      chunks: 'all',
    },
  },

  resolve: {
    modules: ['node_modules', ROOT_PATH, 'app'],
    extensions: ['.json', '.tsx', '.ts', '.js'],
    alias: {},
  },

  plugins: [
    new ForkTsCheckerWebpackPlugin({
      watch: [APP_PATH, './app', './core'],
      checkSyntacticErrors: true,
      memoryLimit:
        process.env.WEBPACK_MEMORY_LIMIT || ForkTsCheckerWebpackPlugin.DEFAULT_MEMORY_LIMIT,
    }),

    new CircularDependencyPlugin({
      exclude: /node_modules/,
      failOnError: true,
      cwd: ROOT_PATH,
    }),

    new HtmlWebpackPlugin({
      title: 'ToDo List',
      template: path.resolve(ROOT_PATH, 'templates/index.ejs'),
    }),

    new webpack.DefinePlugin({}),
  ],

  performance: { hints: false },
};
