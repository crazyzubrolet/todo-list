const path = require('path');
require('colors');

const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || 1337;
const ROOT_PATH = path.resolve(__dirname, '..');

const CLIENT_APP_PATH = path.resolve(ROOT_PATH, 'app');
const OUTPUT_PATH = path.join(ROOT_PATH, 'dist', process.env.OUTPUT_PATH || '');

module.exports = {
  HOST,
  PORT,
  ROOT_PATH,
  OUTPUT_PATH,
  CLIENT_APP_PATH,
};
