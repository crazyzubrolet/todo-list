const app = require('express')();
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
require('colors');

const devConfig = require('./dev.config');

const PORT = process.env.PORT || 1337;

const compiler = webpack(devConfig);

const logRequest = request => {
  console.info('--------------------------------------------------------------------------------');
  console.info(`➡ ${new Date().toLocaleString().green} ${request.originalUrl.magenta}`);
  if (request.headers['user-agent']) {
    console.info(`${request.headers['user-agent'].cyan}`);
  }
};

app.get('/', (request, response, next) => {
  logRequest(request);

  next();
});

app.use(
  webpackDevMiddleware(compiler, {
    watchOptions: {
      aggregateTimeout: Number(process.env.WEBPACK_WATCH_AGGREGATE_TIMEOUT) || 300,
      poll: process.env.WEBPACK_WATCH_POLL || true,
    },
    stats: {
      all: false,
      errors: true,
      moduleTrace: true,
      colors: true,
      warningsFilter: /export .* was not found in/,
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    serverSideRender: false,
    publicPath: devConfig.output.publicPath || '/',
    lazy: false,
  })
);

app.listen(PORT || 1337, error => {
  if (error) {
    console.error(error);
  } else {
    console.info(
      `\n${'===>'.green} 🌏  Webpack dev server launched on ${
        `${devConfig.output.publicPath}`.cyan.underline
      }`
    );
  }
});
