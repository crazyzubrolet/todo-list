import { mergeDeepRight } from './utils';

describe('mergeDeepRight', () => {
  const config1 = {
    string: 'text',
    number: 42,
    array: [1, 2, 3],
    nestedObject: {
      array: [1, 2, 3],
      string: 'temp',
    },
    missingField: 'temp',
    nullableField: 'not null',
  };

  const config2 = {
    string: 'test',
    number: 1337,
    array: [2, 4, 6],
    nestedObject: {
      array: [1, 2, 3, 4, 5],
      string: 'actual',
    },
    nullableField: null,
    extraField: {
      value: [],
    },
  };

  it('Should merge two objects with priority to second object', () => {
    const expected = {
      string: 'test',
      number: 1337,
      array: [1, 2, 3, 4, 6],
      nestedObject: {
        array: [1, 2, 3, 4, 5],
        string: 'actual',
      },
      missingField: 'temp',
      nullableField: null,
      extraField: {
        value: [],
      },
    };

    expect(mergeDeepRight(config1, config2)).toEqual(expected);
  });
});
