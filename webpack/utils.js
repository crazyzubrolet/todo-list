const R = require('ramda');

const mergeDeepRight = R.mergeDeepWith(
  R.ifElse(
    Array.isArray,
    R.union,
    /**
     * @param {object} _x
     * @param {object} y
     */
    (_x, y) => y
  )
);

module.exports = {
  mergeDeepRight,
};
