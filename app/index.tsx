import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import configureStore from 'config/redux/configureStore';

import 'normalize.css';
import './index.css';

import App from 'containers/App';

function runApp() {
  const root = document.getElementById('root') || document.createElement('div');
  const { store, persistor } = configureStore({});

  ReactDOM.render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>,
    root
  );
}

if (__DEV__) {
  setTimeout(runApp, 300);
} else {
  runApp();
}
