import { isAnyValuePresent, allKeysHaveValues, trimify } from './utils';

describe('Form utils', () => {
  describe('isAnyValuePresent', () => {
    it('Should return false if all values are empty strings', () => {
      const obj = {
        value1: '',
        value2: '',
        value3: '',
      };

      expect(isAnyValuePresent(obj)).toBe(false);
    });

    it('Should return true if any value is not empty', () => {
      const obj = {
        value1: '',
        value2: 'not empty',
        value3: '',
      };

      expect(isAnyValuePresent(obj)).toBe(true);
    });

    it('Should return false if all values are empty: string, array or undefined', () => {
      const obj = {
        value1: '',
        value2: [],
        value3: undefined,
      };

      expect(isAnyValuePresent(obj)).toBe(false);
    });

    it('Should treat `null` as a value and return true in that case', () => {
      const obj = {
        value1: '',
        value2: null,
        value3: undefined,
      };

      expect(isAnyValuePresent(obj)).toBe(true);
    });
  });

  describe('allKeysHaveValues', () => {
    it('Should return true if all listed keys have values', () => {
      const obj = {
        value1: '',
        value2: null,
        value3: 'text',
      };

      expect(allKeysHaveValues(obj, ['value3'])).toBe(true);
      expect(allKeysHaveValues(obj, ['value2', 'value3'])).toBe(true);
    });

    it('Should return false if any of listed keys is empty', () => {
      const obj1 = {
        value1: 42,
        value2: undefined,
        value3: 'text',
      };
      const obj2 = {
        value1: '',
        value2: [42],
        value3: 'text',
      };

      expect(allKeysHaveValues(obj1, ['value1', 'value2', 'value3'])).toBe(false);
      expect(allKeysHaveValues(obj2, ['value1', 'value2', 'value3'])).toBe(false);
    });

    it('Should treat `null` as a value and return true', () => {
      const obj = {
        value1: 42,
        value2: null,
        value3: 'text',
      };

      expect(allKeysHaveValues(obj, ['value1', 'value2', 'value3'])).toBe(true);
    });
  });

  describe('trimify', () => {
    it.each<any>([
      ['  spaces at the beginning', 'spaces at the beginning'],
      ['spaces at the end  ', 'spaces at the end'],
      ['  spaces at both ends  ', 'spaces at both ends'],
      [42, 42],
      [{ text: 'not a string at all' }, { text: 'not a string at all' }],
    ])(
      "should trim '%s' if it's a string, otherwise it should return that value",
      (input, expected) => {
        expect(trimify(input)).toEqual(expected);
      }
    );
  });
});
