import * as R from 'ramda';

export const isAnyValuePresent = R.compose(
  R.not,
  R.all(R.either(R.isEmpty, R.identical(undefined))),
  R.values
);

export const trimify: (x: any) => any = R.when(R.is(String), R.trim);

export const allKeysHaveValues: <T = {}>(
  x: T,
  keys: Array<Extract<keyof T, string>>
) => boolean = R.compose(
  R.none(R.either(R.isEmpty, R.identical(undefined))),
  R.map(trimify),
  R.values,
  R.flip(R.pickAll)
);
