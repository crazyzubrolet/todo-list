import React from 'react';
import cn from 'classnames';
import * as R from 'ramda';

import { Input } from 'core/components/atoms/Input';

import { omitEmptyKeys } from 'core/utils/object';
import { isLeapYear } from 'core/utils/time';
import { noop } from 'core/utils/noop';

import { allKeysHaveValues, trimify } from './utils';
import styles from './styles.css';

interface IDefaultProps {
  onError(error: FormError): void;
  onSubmit(event: React.FormEvent<HTMLFormElement>, value: IFormResult): void;
}

interface IProps extends IDefaultProps {
  name?: string;
  className?: string;
}

export enum FormError {
  EMPTY = 'empty',
}

const TEXT_INPUT_NAME = 'text_field';
const DATE_INPUT_NAME = 'date_field';

interface IState {
  textFieldValue: string;
  dateFieldValue: string;
  error?: FormError;
}

export interface IFormResult extends Omit<IState, 'error'> {}

const MAX_TIME_DIVERGENCE =
  (isLeapYear(new Date().getFullYear()) ? 366 : 365) * 24 * 60 * 60 * 1000;

class Form extends React.Component<IProps, IState> {
  public static defaultProps: IDefaultProps = {
    onError: noop,
    onSubmit: noop,
  };

  public readonly state: IState = {
    textFieldValue: '',
    dateFieldValue: '',
  };

  private handleSubmit: React.FormEventHandler<HTMLFormElement> = event => {
    const { onSubmit, onError } = this.props;

    const requiredFields = R.omit(['error'], this.state);

    if (allKeysHaveValues(requiredFields, ['textFieldValue', 'dateFieldValue'])) {
      this.setState(
        {
          error: undefined,
        },
        () => onSubmit(event, omitEmptyKeys(R.map<object, any>(trimify, this.state)))
      );
    } else {
      this.setState(
        {
          error: FormError.EMPTY,
        },
        () => onError(FormError.EMPTY)
      );
    }

    event.preventDefault();
  };

  private handleInputChange: React.ChangeEventHandler<HTMLInputElement> = event => {
    const { value, name } = event.target;

    switch (name) {
      case TEXT_INPUT_NAME:
        this.setState({
          textFieldValue: value,
        });
        break;
      case DATE_INPUT_NAME:
        this.setState({
          dateFieldValue: value,
        });
        break;
      default:
        break;
    }
  };

  public render() {
    const { name, className } = this.props;
    const { textFieldValue: textField, dateFieldValue: dateField } = this.state;

    /**
     * Acceptable format for native input without timezone and seconds
     * @example 2011-10-05T14:48:00.000Z -> 2011-10-05T14:48
     */
    const minDate = new Date().toISOString().slice(0, -8);
    const maxDate = new Date(Date.now() + MAX_TIME_DIVERGENCE).toISOString().slice(0, -8);

    return (
      <form name={name} className={cn(styles.form, className)} onSubmit={this.handleSubmit}>
        <Input
          name={TEXT_INPUT_NAME}
          value={textField}
          placeholder="What needs to be done?"
          onChange={this.handleInputChange}
          className={styles.textField}
          required
        />
        <Input
          name={DATE_INPUT_NAME}
          type="datetime-local"
          min={minDate}
          max={maxDate}
          value={dateField}
          className={styles.dateField}
          onChange={this.handleInputChange}
          required
        />
      </form>
    );
  }
}

export default Form;
