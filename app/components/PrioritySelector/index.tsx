import React from 'react';
import cn from 'classnames';

import { Priority } from 'core/types/priority';
import { noop } from 'core/utils/noop';

import styles from './styles.css';

interface IDefaultProps {
  initialPriority: Priority;
  onSelect(priority: Priority): void;
}

interface IProps extends IDefaultProps {
  id: string;
  className?: string;
}

class PrioritySelector extends React.PureComponent<IProps> {
  public static defaultProps: IDefaultProps = {
    onSelect: noop,
    initialPriority: Priority.NONE,
  };

  private onPriorityChange: React.ChangeEventHandler<HTMLInputElement> = event => {
    const { initialPriority, onSelect } = this.props;
    let priority = initialPriority;

    switch (parseInt(event.target.value, 10)) {
      case Priority.HIGH:
        priority = Priority.HIGH;
        break;
      case Priority.MEDIUM:
        priority = Priority.MEDIUM;
        break;
      case Priority.LOW:
        priority = Priority.LOW;
        break;
      case Priority.NONE:
        priority = Priority.NONE;
        break;
      default:
        break;
    }

    onSelect(priority);
  };

  private renderInputByPriority(priority: Priority) {
    const { id } = this.props;
    let specialClassName: string | undefined;
    let title: string;
    const inputId = `${id}_${priority}`;

    switch (priority) {
      case Priority.HIGH:
        specialClassName = styles.high;
        title = 'High priority';
        break;
      case Priority.MEDIUM:
        specialClassName = styles.medium;
        title = 'Medium priority';
        break;
      case Priority.LOW:
        specialClassName = styles.low;
        title = 'Low priority';
        break;
      default:
        title = 'Very low priority';
        break;
    }

    return (
      <label title={title} htmlFor={inputId} className={cn(styles.radioLabel, specialClassName)}>
        <input
          id={inputId}
          value={priority}
          name="priority"
          type="radio"
          className={styles.radioInput}
          onChange={this.onPriorityChange}
        />
      </label>
    );
  }

  public render() {
    const { className } = this.props;

    return (
      <div className={cn(styles.selector, className)}>
        {this.renderInputByPriority(Priority.HIGH)}
        {this.renderInputByPriority(Priority.MEDIUM)}
        {this.renderInputByPriority(Priority.LOW)}
        {this.renderInputByPriority(Priority.NONE)}
      </div>
    );
  }
}

export default PrioritySelector;
