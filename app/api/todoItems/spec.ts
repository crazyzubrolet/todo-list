import { IExternalTodoItem } from 'core/models/todoItem/mappers';
import { Priority } from 'core/types/priority';
import { Status } from 'core/types/status';

import { ITodoItemState } from 'redux/todoList';

import { convertToExternalData } from './utils';

describe('convertToExternalData', () => {
  it('should convert object with data to external items', () => {
    const sampleData = {
      id: 10,
      priority: Priority.NONE,
      title: 'test',
      completed: false,
      dueDate: 0,
    };

    const objectWithData: Record<number, ITodoItemState> = {
      10: { data: sampleData, status: Status.LOADED },
      20: { data: sampleData, status: Status.LOADED },
      30: { data: sampleData, status: Status.LOADED },
    };

    const actual = convertToExternalData(objectWithData);
    const expectedItem: IExternalTodoItem = {
      id: 10,
      priority: 'none',
      title: 'test',
      completed: false,
      due_date: 0,
    };

    expect(actual).toHaveLength(3);
    actual.map(item => expect(item).toEqual(expectedItem));
  });
});
