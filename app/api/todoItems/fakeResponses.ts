import * as R from 'ramda';

import { IExternalTodoItem, mapPriorityToExternal } from 'core/models/todoItem/mappers';
import { ApiResponseType, HttpStatusCode } from 'core/services/api';
import { omitEmptyKeys } from 'core/utils/object';
import { uniqueId } from 'core/utils/uniqueId';

import {
  ICreateParams,
  IUpdateParams,
  ISuccessGetResponse,
  ISuccessCreateResponse,
  IUpdateResponse,
  IDeleteResponse,
} from './types';

import { fixtures } from './initFakeApi';

export const successGetResponse: ISuccessGetResponse = {
  type: ApiResponseType.SUCCESS,
  data: fixtures,
};

export const successCreateResponse = (params: ICreateParams): ISuccessCreateResponse => {
  const { title, priority, dueDate, description } = params;

  const newTodo: IExternalTodoItem = {
    title,
    description,
    due_date: Math.floor(dueDate / 1000),
    completed: false,
    priority: mapPriorityToExternal(priority),
    id: parseInt(uniqueId(''), 10),
  };
  fixtures.push(newTodo);

  return {
    type: ApiResponseType.SUCCESS,
    data: newTodo,
  };
};

export const successUpdateResponse = (id: number, todo: IUpdateParams): IUpdateResponse => {
  const { dueDate, ...restProps } = todo;

  const todoIndex = fixtures.findIndex(todoItem => todoItem.id === id);

  if (todoIndex !== -1) {
    fixtures[todoIndex] = R.mergeRight(
      fixtures[todoIndex],
      omitEmptyKeys({
        ...restProps,
        due_date: dueDate,
      })
    );

    return {
      type: ApiResponseType.SUCCESS,
      data: 200,
    };
  }

  return {
    type: ApiResponseType.CLIENT_ERROR,
    description: `Item with id: ${id} was not found`,
    response: {
      body: {},
      status: HttpStatusCode.BAD_REQUEST,
    },
  };
};

export const successDeleteResponse = (id: number): IDeleteResponse => {
  const todoIndex = fixtures.findIndex(todo => todo.id === id);

  if (todoIndex !== -1) {
    fixtures.splice(todoIndex, 1);

    return {
      type: ApiResponseType.SUCCESS,
      data: 200,
    };
  }

  return {
    type: ApiResponseType.CLIENT_ERROR,
    description: `Item with id: ${id} was not found`,
    response: {
      body: {},
      status: HttpStatusCode.BAD_REQUEST,
    },
  };
};
