import * as R from 'ramda';

import { IExternalTodoItem, mapPriorityToExternal } from 'core/models/todoItem/mappers';
import { ITodoItem } from 'core/models/todoItem';

import { ITodoItemState } from 'redux/todoList';

export const convertToExternalData = R.compose(
  R.map(
    ({ dueDate, priority, ...rest }: ITodoItem): IExternalTodoItem => ({
      ...rest,
      priority: mapPriorityToExternal(priority),
      due_date: Math.floor(dueDate / 1000),
    })
  ),
  R.filter(Boolean),
  R.map((todo: ITodoItemState) => todo.data),
  R.values
);
