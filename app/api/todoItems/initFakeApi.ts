import { IExternalTodoItem } from 'core/models/todoItem/mappers';

import { idCounter } from 'core/utils/uniqueId';
import { IGlobalState } from 'config/redux/types';

import { convertToExternalData } from './utils';

export const fixtures: IExternalTodoItem[] = [];

export default function initFakeApi(state: IGlobalState['todoList']) {
  convertToExternalData(state.byId).forEach(item => {
    fixtures.push(item);
    idCounter[''] = !idCounter[''] || item.id > idCounter[''] ? item.id : idCounter[''];
  });
}
