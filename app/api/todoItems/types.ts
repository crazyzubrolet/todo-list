import { ITransportSuccess, ITransportError } from 'core/services/api';
import { ITodoItem } from 'core/models/todoItem';
import { IExternalTodoItem } from 'core/models/todoItem/mappers';

export const API_PATH = '/todo_list';

export type ISuccessGetResponse = ITransportSuccess<IExternalTodoItem[]>;

export type ICreateParams = Omit<ITodoItem, 'id' | 'completed'>;

export type ISuccessCreateResponse = ITransportSuccess<IExternalTodoItem>;

export type IUpdateParams = Partial<Omit<ITodoItem, 'id'>>;

export type IUpdateResponse = ITransportSuccess<any> | ITransportError;

export type IDeleteResponse = ITransportSuccess<any> | ITransportError;
