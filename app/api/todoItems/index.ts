import { RequestMethod, ApiService } from 'core/services/api';
import { mapExternalTodoItem } from 'core/models/todoItem/mappers';

import {
  API_PATH,
  ISuccessGetResponse,
  ISuccessCreateResponse,
  ICreateParams,
  IUpdateParams,
} from './types';

export function getTodoItems(apiService: ApiService) {
  return apiService
    .sendRequest({
      path: API_PATH,
      method: RequestMethod.GET,
    })
    .then(({ data }: ISuccessGetResponse) => data.map(mapExternalTodoItem));
}

export function createTodoItem(apiService: ApiService, todo: ICreateParams) {
  return apiService
    .sendRequest({
      path: API_PATH,
      method: RequestMethod.POST,
      body: {
        todo,
      },
    })
    .then(({ data }: ISuccessCreateResponse) => mapExternalTodoItem(data));
}

export function updateTodoItem(apiService: ApiService, id: number, todo: IUpdateParams) {
  return apiService.sendRequest({
    path: `${API_PATH}/${id}`,
    method: RequestMethod.PATCH,
    body: {
      todo,
    },
  });
}

export function deleteTodoItem(apiService: ApiService, id: number) {
  return apiService.sendRequest({
    path: `${API_PATH}/${id}`,
    method: RequestMethod.DELETE,
  });
}
