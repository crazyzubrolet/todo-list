/* eslint-disable no-underscore-dangle */
declare module '*.css';

declare const __DEV__: boolean;

declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

declare type NonNullableObject<T> = { [P in keyof T]-?: NonNullable<T[P]> };

declare type ComponentProps<T> = T extends React.ComponentType<infer P> | React.Component<infer P>
  ? JSX.LibraryManagedAttributes<T, P>
  : never;
