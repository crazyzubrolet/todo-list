import * as R from 'ramda';

declare module 'ramda' {
  // tslint:disable-next-line interface-name
  interface Filter {
    <T, S extends T>(fn: (value: T) => value is S): FilterOnceApplied<S>;
    <T>(fn: (value: T) => boolean): FilterOnceApplied<T>;
    <T, Kind extends 'array', S extends T>(fn: (value: T) => value is S): (
      list: ReadonlyArray<T>
    ) => S[];
    <T, Kind extends 'array'>(fn: (value: T) => boolean): (list: ReadonlyArray<T>) => T[];
    <T, Kind extends 'object', S extends T>(fn: (value: T) => value is S): (
      list: Dictionary<T>
    ) => Dictionary<S>;
    <T, Kind extends 'object'>(fn: (value: T) => boolean): (list: Dictionary<T>) => Dictionary<T>;
    <T, S extends T>(fn: (value: T) => value is S, list: ReadonlyArray<T>): S[];
    <T>(fn: (value: T) => boolean, list: ReadonlyArray<T>): T[];
    <T, S extends T>(fn: (value: T) => value is S, obj: Dictionary<T>): Dictionary<S>;
    <T>(fn: (value: T) => boolean, obj: Dictionary<T>): Dictionary<T>;
  }
}
