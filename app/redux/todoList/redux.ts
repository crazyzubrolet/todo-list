import * as R from 'ramda';

import { ActionPromise, handleActions, handleAsyncAction } from 'core/utils/redux';
import { Status } from 'core/types/status';
import { ApiService, MockTransport } from 'core/services/api';

import {
  getTodoItems as apiGetTodoItems,
  createTodoItem as apiCreateTodoItem,
  updateTodoItem as apiUpdateTodoItem,
  deleteTodoItem as apiDeleteTodoItem,
} from 'api/todoItems';
import {
  successGetResponse,
  successCreateResponse,
  successUpdateResponse,
  successDeleteResponse,
} from 'api/todoItems/fakeResponses';
import { API_PATH } from 'api/todoItems/types';

import {
  IGetTodosAction,
  ICreateTodoAction,
  IUpdateTodoAction,
  ActionType,
  IState,
  IDeleteTodoAction,
} from './types';

export function getTodos(): ActionPromise<IGetTodosAction> {
  return {
    type: ActionType.GET_TODOS,
    payload: apiGetTodoItems(
      ApiService.create(
        MockTransport.create({
          [API_PATH]: successGetResponse,
        })
      )
    ),
  };
}

export function createTodo(
  todo: Parameters<typeof apiCreateTodoItem>[1]
): ActionPromise<ICreateTodoAction> {
  return {
    type: ActionType.CREATE_TODO,
    payload: apiCreateTodoItem(
      ApiService.create(
        MockTransport.create({
          [API_PATH]: successCreateResponse(todo),
        })
      ),
      todo
    ),
  };
}

export function updateTodo(
  id: number,
  todo: Parameters<typeof apiUpdateTodoItem>[2]
): ActionPromise<IUpdateTodoAction> {
  return {
    type: ActionType.UPDATE_TODO,
    meta: {
      id,
      item: todo,
    },
    payload: apiUpdateTodoItem(
      ApiService.create(
        MockTransport.create({
          [`${API_PATH}/${id}`]: successUpdateResponse(id, todo),
        })
      ),
      id,
      todo
    ),
  };
}

export function deleteTodo(id: number): ActionPromise<IDeleteTodoAction> {
  return {
    type: ActionType.DELETE_TODO,
    meta: {
      id,
    },
    payload: apiDeleteTodoItem(
      ApiService.create(
        MockTransport.create({
          [`${API_PATH}/${id}`]: successDeleteResponse(id),
        })
      ),
      id
    ),
  };
}

const initialState: IState = {
  byId: {},
  ids: [],
  isLoading: false,
  error: undefined,
};

type IActions = IGetTodosAction | ICreateTodoAction | IUpdateTodoAction | IDeleteTodoAction;

export default handleActions<IState, IActions>(
  {
    [ActionType.GET_TODOS]: handleAsyncAction({
      request: state => ({
        ...state,
        isLoading: true,
      }),
      success: (state, { payload }) => ({
        isLoading: false,
        byId: R.reduce(
          (acc, todo) => ({
            ...acc,
            [todo.id]: {
              data: todo,
              status: Status.LOADED,
              error: undefined,
            },
          }),
          state.byId,
          payload
        ),
        ids: R.union(state.ids, R.map(todo => todo.id, payload)),
      }),
      failure: (state, { payload }) => ({
        ...state,
        isLoading: false,
        error: payload,
      }),
    }),

    [ActionType.CREATE_TODO]: handleAsyncAction({
      request: state => ({
        ...state,
        isLoading: true,
        error: undefined,
      }),
      success: (state, { payload }) => ({
        ...state,
        byId: {
          ...state.byId,
          [payload.id]: {
            data: payload,
            status: Status.LOADED,
          },
        },
        isLoading: false,
        ids: R.union(state.ids, [payload.id]),
      }),
      failure: (state, { payload }) => ({
        ...state,
        isLoading: false,
        error: payload,
      }),
    }),

    [ActionType.UPDATE_TODO]: handleAsyncAction({
      request: (state, { meta }): IState => ({
        ...state,
        byId: {
          ...state.byId,
          [meta.id]: {
            data: {
              ...state.byId[meta.id].data,
              ...(meta.item as any),
            },
            status: Status.UPDATING,
            error: undefined,
          },
        },
        isLoading: true,
        error: undefined,
      }),
      success: (state, { meta }) => ({
        ...state,
        byId: {
          ...state.byId,
          [meta.id]: {
            ...(state.byId[meta.id] as any),
            status: Status.LOADED,
          },
        },
        isLoading: false,
      }),
      failure: (state, { meta, payload }) => ({
        ...state,
        byId: {
          ...state.byId,
          [meta.id]: {
            ...state.byId[meta.id],
            status: Status.ERROR,
            error: payload,
          },
        },
        isLoading: false,
      }),
    }),

    [ActionType.DELETE_TODO]: handleAsyncAction({
      success: (state, { meta }) => ({
        ...state,
        byId: {
          ...state.byId,
          [meta.id]: {
            status: Status.INITIAL,
          },
        },
        ids: R.without([meta.id], state.ids),
      }),
    }),
  },
  initialState
);
