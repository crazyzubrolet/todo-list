import { IExternalTodoItem } from 'core/models/todoItem/mappers';
import { ApiResponseType } from 'core/services/api';
import { Priority } from 'core/types/priority';

import { ISuccessGetResponse } from 'api/todoItems/types';

import { IState } from '../types';

export const initialState: IState = {
  byId: {},
  ids: [],
  isLoading: false,
  error: undefined,
};

export const newItem = {
  title: 'Test',
  dueDate: 0,
  priority: Priority.HIGH,
  description: 'temp unused text',
};

export const sampleExternalData: IExternalTodoItem[] = [
  {
    id: 449,
    title: 'sdasda',
    due_date: 1555456320,
    priority: 'low',
    completed: false,
  },
  {
    id: 551,
    title: 'sdasda',
    due_date: 1555456320,
    priority: 'high',
    completed: true,
  },
  {
    id: 585,
    title: 'sdasda',
    due_date: 1555456320,
    priority: 'high',
    completed: false,
  },
];

export const sampleGetTodosResponse: ISuccessGetResponse = {
  type: ApiResponseType.SUCCESS,
  data: sampleExternalData,
};
