import { IGlobalState } from 'config/redux/types';

import { Priority } from 'core/types/priority';
import { Status } from 'core/types/status';

import { IThunkStore, setupStore, flushAllPromises } from 'redux/helpers';
import { fixtures } from 'api/todoItems/initFakeApi';

import todoList, { getTodos, createTodo, updateTodo, deleteTodo } from '../redux';
import { ActionType } from '../types';
import {
  selectTodoItems,
  selectTodoItemState,
  selectTodoItemsIds,
  selectSortedItemsIds,
} from '../selectors';

import { initialState, sampleExternalData, newItem } from './fixtures';

async function createTodoInStore(store: IThunkStore) {
  store.dispatch(createTodo(newItem));
  await flushAllPromises();
}

describe('Todo list reducer', () => {
  let store: IThunkStore;

  beforeEach(() => {
    store = setupStore({ todoList } as any);
  });

  afterEach(() => {
    fixtures.length = 0;
  });

  it('should return initial state', () => {
    const actual = store.getState();
    const expected: Partial<IGlobalState> = {
      todoList: initialState,
    };

    expect(actual).toEqual(expected);
  });

  it(`should handle ${ActionType.GET_TODOS} action`, async () => {
    fixtures.push(...sampleExternalData);

    store.dispatch(getTodos());
    await flushAllPromises();

    expect(store.getState()).toMatchSnapshot();
  });

  it(`should handle ${ActionType.CREATE_TODO} action`, async () => {
    await createTodoInStore(store);

    expect(store.getState()).toMatchSnapshot();
  });

  it(`should handle ${ActionType.UPDATE_TODO} action`, async () => {
    await createTodoInStore(store);
    expect(store.getState()).toMatchSnapshot();

    const updatedStore = store.getState().todoList;
    const existingItem = updatedStore.byId[updatedStore.ids[0]].data!;
    const updateData: Parameters<typeof updateTodo>[1] = {
      completed: !existingItem.completed,
      dueDate: 0,
      priority: Priority.HIGH,
      title: 'Test title',
      description: 'unused text',
    };

    store.dispatch(updateTodo(existingItem.id, updateData));
    await flushAllPromises();

    expect(store.getState()).toMatchSnapshot();
  });

  it(`should handle ${ActionType.DELETE_TODO} action`, async () => {
    await createTodoInStore(store);
    const updatedStore = store.getState();
    expect(updatedStore).toMatchSnapshot();

    const idToRemove = updatedStore.todoList.ids[0];
    store.dispatch(deleteTodo(idToRemove));
    await flushAllPromises();

    expect(store.getState()).toMatchSnapshot();
  });
});

describe('Selectors', () => {
  let store: IThunkStore;

  beforeEach(async () => {
    store = setupStore({ todoList } as any);

    await createTodoInStore(store);
  });

  afterEach(() => {
    fixtures.length = 0;
  });

  it('should select from store state', async () => {
    let state = store.getState();

    expect(selectTodoItemsIds(state)).toHaveLength(1);

    const itemId = selectTodoItemsIds(state)[0];
    const itemState = selectTodoItemState(state, itemId);

    expect(itemState).toHaveProperty('status', Status.LOADED);
    expect(itemState.error).toBeUndefined();
    Object.keys(newItem).forEach(key => {
      expect(itemState.data).toHaveProperty(key, (newItem as any)[key]);
    });

    const items = selectTodoItems(state);

    expect(items).toHaveLength(1);
    Object.keys(newItem).forEach(key => {
      expect(items[0]).toHaveProperty(key, (newItem as any)[key]);
    });

    store.dispatch(createTodo({ ...newItem, priority: Priority.LOW, dueDate: 1 }));
    await flushAllPromises();
    state = store.getState();

    const sortedByPriorityDesc = selectSortedItemsIds({
      desc: true,
      sortField: 'priority',
    })(state);

    expect(sortedByPriorityDesc).toHaveLength(2);

    let item1 = selectTodoItemState(state, sortedByPriorityDesc[0]);
    let item2 = selectTodoItemState(state, sortedByPriorityDesc[1]);

    expect(item1.data!.priority).toBeGreaterThanOrEqual(item2.data!.priority);

    const sortedByPriorityAsc = selectSortedItemsIds({
      desc: false,
      sortField: 'priority',
    })(state);

    item1 = selectTodoItemState(state, sortedByPriorityAsc[0]);
    item2 = selectTodoItemState(state, sortedByPriorityAsc[1]);

    expect(item1.data!.priority).toBeLessThanOrEqual(item2.data!.priority);

    const sortedByDueDateDesc = selectSortedItemsIds({
      desc: true,
      sortField: 'dueDate',
    })(state);

    item1 = selectTodoItemState(state, sortedByDueDateDesc[0]);
    item2 = selectTodoItemState(state, sortedByDueDateDesc[1]);

    expect(item1.data!.dueDate).toBeGreaterThanOrEqual(item2.data!.dueDate);
  });
});
