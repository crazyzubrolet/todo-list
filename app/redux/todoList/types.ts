import { ITodoItem } from 'core/models/todoItem';
import { Status } from 'core/types/status';

export enum ActionType {
  GET_TODOS = 'core/todoList/GET_TODOS',
  CREATE_TODO = 'core/todoList/CREATE_TODO',
  UPDATE_TODO = 'core/todoList/UPDATE_TODO',
  DELETE_TODO = 'core/todoList/DELETE_TODO',
}

export type ITodoItemState =
  | {
      status: Status.INITIAL | Status.LOADING;
      data?: ITodoItem;
      error?: undefined;
    }
  | {
      status: Status.ERROR;
      data?: ITodoItem;
      error: any;
    }
  | {
      status: Status.UPDATING;
      data: ITodoItem;
      error?: undefined;
    }
  | {
      status: Status.LOADED;
      data: ITodoItem;
      error?: undefined;
    };

export interface IState {
  byId: Record<number, ITodoItemState>;
  isLoading: boolean;
  ids: number[];
  error?: any;
}

export interface IGetTodosAction {
  type: ActionType.GET_TODOS;
  payload: ITodoItem[];
}

export interface ICreateTodoAction {
  type: ActionType.CREATE_TODO;
  payload: ITodoItem;
}

export interface IUpdateTodoAction {
  type: ActionType.UPDATE_TODO;
  meta: {
    id: number;
    item: Partial<ITodoItem>;
  };
  payload: any;
}

export interface IDeleteTodoAction {
  type: ActionType.DELETE_TODO;
  meta: {
    id: number;
  };
  payload: any;
}
