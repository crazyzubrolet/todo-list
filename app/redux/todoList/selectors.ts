import * as R from 'ramda';
import { Status } from 'core/types/status';

import { ITodoItem } from 'core/models/todoItem';
import { IGlobalState } from 'config/redux/types';
import { ITodoItemState } from './types';

export const selectTodoItemState = (state: IGlobalState, id: number): ITodoItemState => {
  const itemState = state.todoList.byId[id];

  return (
    itemState || {
      status: Status.INITIAL,
    }
  );
};

export const selectTodoItems = (state: IGlobalState) =>
  R.compose(
    R.map((item: ITodoItemState) => item.data),
    R.values
  )(state.todoList.byId);

export const selectTodoItemsIds = (state: IGlobalState) => state.todoList.ids;

const sorting = (desc: boolean = true) => R.ifElse(() => desc, R.descend, R.ascend);

const selectItemsSortedBy = (
  state: IGlobalState,
  {
    sortField,
    desc,
  }: { sortField: Extract<keyof ITodoItem, 'id' | 'priority' | 'dueDate'>; desc?: boolean }
) =>
  R.sort(
    R.compose(
      sorting(desc),
      R.prop
    )(sortField),
    selectTodoItems(state)
  );

export const selectSortedItemsIds = (options: Parameters<typeof selectItemsSortedBy>[1]) => (
  state: IGlobalState
) =>
  R.map(
    (todo: ITodoItem) => todo.id,
    R.filter(
      (item: ITodoItem | undefined): item is ITodoItem => !!item,
      selectItemsSortedBy(state, options)
    )
  );
