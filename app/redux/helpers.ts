import thunk from 'redux-thunk';
import {
  applyMiddleware,
  createStore,
  combineReducers,
  ReducersMapObject,
  Store,
  AnyAction,
} from 'redux';

import { IThunkDispatch, IGlobalState, IThunkExtraArguments } from 'config/redux/types';
import asyncActionMiddleware from 'redux/middlewares/asyncAction';

export const flushAllPromises = () => new Promise(resolve => setImmediate(resolve));

export const setupStore = (reducers: ReducersMapObject) => {
  const middlewares = [
    asyncActionMiddleware(),
    thunk.withExtraArgument<IThunkExtraArguments>({ getStore: () => store }),
  ];

  const store: Store<any, AnyAction> = createStore(
    combineReducers(reducers),
    {},
    applyMiddleware(...middlewares)
  );

  return store;
};

export type IThunkStore = Store<IGlobalState> & {
  dispatch: IThunkDispatch;
};
