import { Dispatch, AnyAction } from 'redux';

const isPromisable = (payload: any): payload is Promise<any> =>
  payload && typeof payload.then === 'function';

function asyncActionMiddleware() {
  return () => (next: Dispatch) => (action: AnyAction) => {
    if (!isPromisable(action.payload)) {
      return next(action);
    }

    next({
      ...action,
      meta: {
        ...(action.meta || {}),
        isPending: true,
      },
    });

    return action.payload
      .then(
        data => {
          next({ ...action, payload: data });
          return data;
        },
        error => {
          next({ ...action, payload: error, error: true });
          return Promise.reject(error);
        }
      )
      .catch(error => {
        throw error;
      });
  };
}

export default asyncActionMiddleware;
