import { Dispatch, Middleware } from 'redux';
import { REHYDRATE } from 'redux-persist';

import initFakeApi from 'api/todoItems/initFakeApi';
import { IGlobalState } from 'config/redux/types';

function fakeApiInitMiddleware(): Middleware<Dispatch, IGlobalState> {
  return () => next => action => {
    if (action.type === REHYDRATE) {
      initFakeApi(action.payload ? action.payload : {});
    }

    return next(action);
  };
}

export default fakeApiInitMiddleware;
