import {
  createStore,
  applyMiddleware,
  compose,
  Store,
  Middleware,
  combineReducers,
  AnyAction,
  DeepPartial,
  Reducer,
} from 'redux';
import { Action } from 'redux-actions';
import { persistStore, persistReducer, createTransform } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import { enableBatching } from 'redux-batched-actions';

import { groupById } from 'core/utils/object';

import asyncActionMiddleware from 'redux/middlewares/asyncAction';
import fakeApiInitMiddleware from 'redux/middlewares/fakeApiInit';
import todoListReducer from 'redux/todoList/redux';

import globalEnvironment from './environment';
import { IGlobalState, IThunkExtraArguments } from './types';

const todoListTranformer = createTransform(
  (inbound: IGlobalState['todoList']['byId']) =>
    Object.values(inbound)
      .map(item => item.data)
      .filter(Boolean),
  outbound => groupById(outbound, { subKey: 'data' }),
  { whitelist: ['byId'] }
);

const reducer: Reducer<IGlobalState> = combineReducers({
  todoList: persistReducer<any, any>(
    {
      key: 'todo-list',
      whitelist: ['byId', 'ids'],
      storage,
      transforms: [todoListTranformer],
    },
    todoListReducer
  ) as () => IGlobalState['todoList'],
});

const rootReducer = (state: IGlobalState, action: Action<AnyAction>) => reducer(state, action);

function configureStore(initialState?: DeepPartial<IGlobalState>) {
  const middlewares: Middleware[] = [
    asyncActionMiddleware(),
    fakeApiInitMiddleware(),
    thunk.withExtraArgument<IThunkExtraArguments>({ getStore: () => store }),
  ];

  const finalCreateStore = compose<any>(
    applyMiddleware(...middlewares),
    // eslint-disable-next-line no-underscore-dangle
    __DEV__ && globalEnvironment.__REDUX_DEVTOOLS_EXTENSION__
      ? // eslint-disable-next-line no-underscore-dangle
        globalEnvironment.__REDUX_DEVTOOLS_EXTENSION__()
      : (f: any) => f
  )(createStore);

  const store: Store<IGlobalState> = finalCreateStore(
    enableBatching(rootReducer as () => IGlobalState),
    initialState
  );

  return { store, persistor: persistStore(store) };
}

export default configureStore;
