import { Store, Action, AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

import { ActionPayload } from 'core/utils/redux';

import { IState as ITodoListState } from 'redux/todoList/types';

export interface IGlobalState {
  todoList: ITodoListState;
}

export interface IThunkExtraArguments {
  getStore(): Store;
}

export type IThunkAction<A extends Action> = ThunkAction<
  ActionPayload<A>,
  IGlobalState,
  IThunkExtraArguments,
  A
>;

export type IThunkDispatch = ThunkDispatch<IGlobalState, IThunkExtraArguments, AnyAction>;
