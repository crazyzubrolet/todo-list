import { StoreEnhancerStoreCreator, StoreEnhancer } from 'redux';

import { IGlobalState } from './types';

interface IGlobalEnvironment extends Window {
  __REDUX_DEVTOOLS_EXTENSION__?(): any;
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?(
    ...enhancers: Array<StoreEnhancerStoreCreator | StoreEnhancer<IGlobalState>>
  ): any;
  IntlPolyfill?: object;
}

const globalEnvironment: IGlobalEnvironment = window;

export default globalEnvironment;
