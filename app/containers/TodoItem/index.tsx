import React from 'react';
import { connect } from 'react-redux';
import cn from 'classnames';

import { TodoItem } from 'core/components/molecules/TodoItem';
import { ITodoItem } from 'core/models/todoItem';
import { Status } from 'core/types/status';
import { noop } from 'core/utils/noop';

import { IGlobalState } from 'config/redux/types';
import { selectTodoItemState } from 'redux/todoList';

import PrioritySelector from 'components/PrioritySelector';
import IconDelete from './IconDelete';

import styles from './styles.css';

type StateProps = 'done' | 'title' | 'priority';

type IOwnProps = {
  id: number;
  onItemChange?(item: Partial<ITodoItem> & { id: number }): void;
  onItemDelete?(id: number): void;
} & Partial<Omit<ComponentProps<typeof TodoItem>, StateProps | 'id' | 'onResolve' | 'dueDate'>>;

interface IStateProps extends Partial<Pick<ComponentProps<typeof TodoItem>, StateProps>> {
  status: Status;
  dueDate?: number;
}

interface IProps extends IOwnProps, IStateProps {}

const itemIsLoaded = (props: IProps): props is IProps & NonNullableObject<IStateProps> =>
  props.status === Status.LOADED || props.status === Status.UPDATING;

const timeFormat: Intl.DateTimeFormatOptions = {
  day: '2-digit',
  month: 'short',
  year: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
};

class ConnectedTodoItem extends React.PureComponent<IProps> {
  private handleItemDelete = () => {
    const { id, onItemDelete = noop } = this.props;

    onItemDelete(id);
  };

  private handleItemChange(key: keyof ITodoItem): (value: ITodoItem[typeof key]) => void {
    const { id, onItemChange = noop } = this.props;

    return value => {
      onItemChange({
        id,
        [key]: value,
      });
    };
  }

  public render() {
    if (!itemIsLoaded(this.props)) {
      return null;
    }

    const { id, dueDate, priority } = this.props;
    const isOutdated = Date.now() > dueDate;
    const date = new Date(dueDate);

    return (
      <div className={cn(styles.container, isOutdated && styles.outdated)}>
        <IconDelete className={styles.iconDelete} onClick={this.handleItemDelete} />
        <TodoItem
          {...this.props}
          id={`todo-item_${id}`}
          dueDate={`${date.toLocaleTimeString(undefined, timeFormat)}`}
          onResolve={this.handleItemChange('completed')}
        />
        <PrioritySelector
          id={`todo-item-priority_${id}`}
          initialPriority={priority}
          className={styles.prioritySelector}
          onSelect={this.handleItemChange('priority')}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IGlobalState, ownProps: IOwnProps): IStateProps => {
  const todoItemState = selectTodoItemState(state, ownProps.id);

  if (todoItemState.status !== Status.LOADED && todoItemState.status !== Status.UPDATING) {
    return {
      status: todoItemState.status,
    };
  }

  const { data, status } = todoItemState;

  return {
    status,
    done: data.completed,
    dueDate: data.dueDate,
    priority: data.priority,
    title: data.title,
  };
};

export default connect(mapStateToProps)(ConnectedTodoItem);
