import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import cn from 'classnames';

import { IGlobalState } from 'config/redux/types';
import { getTodos, updateTodo, deleteTodo, selectTodoItemsIds } from 'redux/todoList';

import TodoItem from 'containers/TodoItem';

import styles from './styles.css';

interface IOwnProps {
  idsSelector?(state: IGlobalState): number[];
  className?: string;
}

interface IStateProps {
  todoList: number[];
}

interface IDispatchProps {
  actions: typeof actionCreators;
}

interface IProps extends IOwnProps, IStateProps, IDispatchProps {}

interface IState {}

const actionCreators = {
  getTodos,
  updateTodo,
  deleteTodo,
};

class TodoList extends React.Component<IProps, IState> {
  public readonly state: IState = {};

  public componentDidMount() {
    const { actions } = this.props;

    actions.getTodos();
  }

  private handleItemDelete: ComponentProps<typeof TodoItem>['onItemDelete'] = id => {
    const { actions } = this.props;

    actions.deleteTodo(id);
  };

  private handleItemChange: ComponentProps<typeof TodoItem>['onItemChange'] = ({ id, ...rest }) => {
    const { actions } = this.props;

    actions.updateTodo(id, rest);
  };

  private renderTodoItems() {
    const { todoList } = this.props;

    return todoList.map(todoId => (
      <TodoItem
        key={todoId}
        id={todoId}
        className={styles.todoItem}
        onItemChange={this.handleItemChange}
        onItemDelete={this.handleItemDelete}
      />
    ));
  }

  public render() {
    const { className } = this.props;

    return <div className={cn(styles.todoList, className)}>{this.renderTodoItems()}</div>;
  }
}

const mapStateToProps = (
  state: IGlobalState,
  { idsSelector = selectTodoItemsIds }: IOwnProps
): IStateProps => ({
  todoList: idsSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch): IDispatchProps => ({
  actions: bindActionCreators(actionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
