import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Title } from 'core/components/atoms/Title';
import { Select } from 'core/components/atoms/Select';
import { Priority } from 'core/types/priority';

import { createTodo, selectSortedItemsIds } from 'redux/todoList';

import TodoList from 'containers/TodoList';
import Form, { FormError } from 'components/Form';

import { ITodoItem } from 'core/models/todoItem';
import styles from './styles.css';

interface IDispatchProps {
  actions: typeof actionCreators;
}

interface IProps extends IDispatchProps {}

interface IState {
  sortOrder: Sort;
  sortField: Extract<keyof ITodoItem, 'priority' | 'dueDate'>;
}

const actionCreators = {
  createTodo,
};

enum Sort {
  ASC = 'Ascending',
  DESC = 'Descending',
}

enum SortField {
  PRIORITY = 'By priority',
  DUE_DATE = 'By due date',
}

class App extends React.Component<IProps, IState> {
  public readonly state: IState = {
    sortField: 'priority',
    sortOrder: Sort.DESC,
  };

  private handleSortChange: ComponentProps<typeof Select>['onSelect'] = event => {
    switch (event.target.value) {
      case Sort.DESC:
        this.setState({
          sortOrder: Sort.DESC,
        });
        break;
      case Sort.ASC:
        this.setState({
          sortOrder: Sort.ASC,
        });
        break;
      case SortField.PRIORITY:
        this.setState({
          sortField: 'priority',
        });
        break;
      case SortField.DUE_DATE:
        this.setState({
          sortField: 'dueDate',
        });
        break;
      default:
        break;
    }
  };

  private handleFormSubmit: ComponentProps<typeof Form>['onSubmit'] = (_event, formData) => {
    const { actions } = this.props;

    actions.createTodo({
      title: formData.textFieldValue,
      dueDate: new Date(formData.dateFieldValue).getTime(),
      priority: Priority.NONE,
    });
  };

  private handleFormError: ComponentProps<typeof Form>['onError'] = error => {
    if (error === FormError.EMPTY) {
      // eslint-disable-next-line no-alert
      alert('All required fields should not be empty');
    }
  };

  public render() {
    const { sortField, sortOrder } = this.state;
    const filter = selectSortedItemsIds({
      desc: sortOrder === Sort.DESC,
      sortField,
    });

    return (
      <div className={styles.home}>
        <header>
          <Title className={styles.title}>Todos</Title>
        </header>
        <main className={styles.main}>
          <Form
            name="todo_item_create"
            className={styles.form}
            onSubmit={this.handleFormSubmit}
            onError={this.handleFormError}
          />
          <TodoList className={styles.todoList} idsSelector={filter} />
          <div className={styles.filters}>
            <Select
              placeholder="Sort by"
              values={[SortField.PRIORITY, SortField.DUE_DATE]}
              className={styles.select}
              onSelect={this.handleSortChange}
            />
            <Select
              placeholder="Sort order"
              values={[Sort.DESC, Sort.ASC]}
              className={styles.select}
              onSelect={this.handleSortChange}
            />
          </div>
        </main>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch): IDispatchProps => ({
  actions: bindActionCreators(actionCreators, dispatch),
});

export default connect(
  null,
  mapDispatchToProps
)(App);
