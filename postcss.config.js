const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

module.exports = ({ options: { isProd } }) => ({
  plugins: [
    autoprefixer({ browsers: ['Chrome > 60'] }),
    isProd && cssnano({ preset: ['default', { normalizeUrl: false }] }),
  ].filter(Boolean),
});
